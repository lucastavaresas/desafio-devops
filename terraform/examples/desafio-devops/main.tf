provider "aws" {
  region = var.region
}

module "this" {
  source = "../../modules/instance"

  cidr_ssh_range = "10.0.0.0/12"
}